package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.InternalDangerImplementation;

import java.util.Collection;
import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class AdminEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();


    @NotNull
    private final AuthenticationEndpointService authenticationEndpointService = new AuthenticationEndpointService();

    @NotNull
    private final AuthenticationEndpoint authEndpoint = authenticationEndpointService.getAuthenticationEndpointPort();


    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @Test
    @TestCaseName("Run testCloseAllSessions for closeAllSessions(session)")
    public void testCloseAllSessions() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final int closeSessions = this.adminEndpoint.closedAllSessions(open);
        Assert.assertEquals(1, closeSessions);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetAllSessions for getAllSessions(session)")
    public void testGetAllSessions() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<SessionDTO> allSessions = this.adminEndpoint.getAllSessions(open);
        Assert.assertNotNull(allSessions);
        Assert.assertNotEquals(0, allSessions.size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testSignUpUserWithUserRole for signUpUserWithUserRole(session, random, random)")
    public void testSignUpUserWithUserRole() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUser = this.adminEndpoint.signUpUserWithUserRole(
                open, login, password, UserRole.USER
        );
        Assert.assertNotNull(addUser);
        Assert.assertEquals(login, addUser.getLogin());
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDeleteUserById for deleteUserById(session, id)")
    public void testDeleteUserById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final int deleteFlag = this.adminEndpoint.deleteUserById(open, addRecord.getId());
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDeleteUserByLogin for deleteUserByLogin(session, login)")
    public void testDeleteUserByLogin() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @Ignore("Делает работу остальных тестов некорректной")
    @Category(InternalDangerImplementation.class)
    @TestCaseName("Run testDeleteAllUsers for deleteAllUsers(session)")
    public void testDeleteAllUsers() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final int deletedAll = this.adminEndpoint.deleteAllUsers(open);
        Assert.assertEquals(1, deletedAll);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testLockUserByLogin for lockUserByLogin(session, login)")
    public void testLockUserByLogin() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserLimitedDTO lockUser = this.adminEndpoint.lockUserByLogin(open, login);
        Assert.assertNotNull(lockUser);
        Assert.assertEquals(addRecord.getId(), lockUser.getId());
        Assert.assertEquals(login, lockUser.getLogin());
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testUnlockUserByLogin for unlockUserByLogin(session, login)")
    public void testUnlockUserByLogin() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @Nullable final UserLimitedDTO lockUser = this.adminEndpoint.lockUserByLogin(open, login);
        Assert.assertNotNull(lockUser);

        @Nullable final UserLimitedDTO unlockUser = this.adminEndpoint.unlockUserByLogin(open, login);
        Assert.assertNotNull(unlockUser);
        Assert.assertEquals(addRecord.getId(), unlockUser.getId());
        Assert.assertEquals(login, unlockUser.getLogin());
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetAllUsers for getAllUsers(session)")
    public void testGetAllUsers() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final Collection<UserLimitedDTO> getUser = this.adminEndpoint.getAllUsers(open);
        Assert.assertNotNull(getUser);
        Assert.assertNotEquals(0, getUser.size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetAllUsersTasks for getAllUsersTasks(session)")
    public void testGetAllUsersTasks() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<TaskDTO> allUsersTasks = this.adminEndpoint.getAllUsersTasks(open);
        Assert.assertNotNull(allUsersTasks);
        Assert.assertNotEquals(0, allUsersTasks.size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetAllUsersProjects for getAllUsersProjects(session)")
    public void testGetAllUsersProjects() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<ProjectDTO> allUsersProjects = this.adminEndpoint.getAllUsersProjects(open);
        Assert.assertNotNull(allUsersProjects);
        Assert.assertNotEquals(0, allUsersProjects.size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetUserById for getUserById(session, login)")
    public void testGetUserById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserLimitedDTO getUser = this.adminEndpoint.getUserById(open, addRecord.getId());
        Assert.assertNotNull(getUser);
        Assert.assertEquals(addRecord.getId(), getUser.getId());
        Assert.assertEquals(login, getUser.getLogin());
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetUserByLogin for getUserByLogin(session, login)")
    public void testGetUserByLogin() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserLimitedDTO getUser = this.adminEndpoint.getUserByLogin(open, addRecord.getLogin());
        Assert.assertNotNull(getUser);
        Assert.assertEquals(addRecord.getId(), getUser.getId());
        Assert.assertEquals(login, getUser.getLogin());
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testEditProfileById for editProfileById(session, id, \"newFirstName\")")
    public void testEditProfileById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserLimitedDTO editUser = this.adminEndpoint.editProfileById(open, addRecord.getId(), "newFirstName");
        Assert.assertNotNull(editUser);
        Assert.assertEquals(addRecord.getId(), editUser.getId());
        Assert.assertEquals(login, editUser.getLogin());
        Assert.assertEquals("newFirstName", editUser.getFirstName());
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testUpdatePasswordById for updatePasswordById(session, id, \"newPassword\")")
    public void testUpdatePasswordById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserLimitedDTO updatePassword = this.adminEndpoint.updatePasswordById(open, addRecord.getId(), "newPassword");
        Assert.assertNotNull(updatePassword);
        Assert.assertEquals(addRecord.getId(), updatePassword.getId());
        Assert.assertEquals(login, updatePassword.getLogin());
        final int deleteFlag = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

}