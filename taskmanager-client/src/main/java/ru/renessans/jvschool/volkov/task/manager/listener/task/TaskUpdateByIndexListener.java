package ru.renessans.jvschool.volkov.task.manager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class TaskUpdateByIndexListener extends AbstractTaskListener {

    @NotNull
    private static final String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";

    @NotNull
    private static final String DESC_TASK_UPDATE_BY_INDEX = "обновить задачу по индексу";

    @NotNull
    private static final String NOTIFY_TASK_UPDATE_BY_INDEX =
            "Происходит попытка инициализации обновления задачи \n" +
                    "Для обновления задачи по индексу введите индекс задачи из списка.\n" +
                    "Для обновления задачи введите её заголовок или заголовок с описанием. ";

    public TaskUpdateByIndexListener(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(taskEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_TASK_UPDATE_BY_INDEX;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_TASK_UPDATE_BY_INDEX;
    }

    @Async
    @Override
    @EventListener(condition = "@taskUpdateByIndexListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_TASK_UPDATE_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();

        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @Nullable final TaskDTO update = super.taskEndpoint.updateTaskByIndex(current, index, title, description);
        ViewUtil.print(update);
    }

}