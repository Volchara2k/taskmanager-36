package ru.renessans.jvschool.volkov.task.manager.controller;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;

import java.util.Objects;

@Controller
@RequiredArgsConstructor
public class ProjectController {

    @NotNull
    private final IProjectUserService projectUserService;

    @NotNull
    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", this.projectUserService.exportOwnerUser());
    }

    @NotNull
    @GetMapping("/project/create")
    public ModelAndView create() {
        return new ModelAndView("project-settable", "project", new Project());
    }

    @NotNull
    @PostMapping("/project/create")
    public String create(
            @ModelAttribute("project") @NotNull final Project project,
            @NotNull final BindingResult result
    ) {
        this.projectUserService.addOwnerUser(project);
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") @NotNull final String id
    ) {
        this.projectUserService.deleteRecordById(id);
        return "redirect:/projects";
    }

    @NotNull
    @SneakyThrows
    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Project project = this.projectUserService.getRecordById(id);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        return new ModelAndView("project-settable", "project", project);
    }

    @NotNull
    @PostMapping("/project/edit/{id}")
    public String edit(
            @ModelAttribute("project") @NotNull final Project project,
            @NotNull final BindingResult result
    ) {
        this.projectUserService.addOwnerUser(project);
        return "redirect:/projects";
    }

}