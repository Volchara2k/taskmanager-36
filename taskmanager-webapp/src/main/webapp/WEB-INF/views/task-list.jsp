<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../include/_header.jsp"/>

<div class="list_header-content">
    <h1>Task list:</h1>
</div>

<div class="main-content">
    <table>

        <tr>
            <th width="200" nowrap="nowrap">ID</th>
            <th width="200" nowrap="nowrap">TITLE</th>
            <th width="100%" align="center">DESCRIPTION</th>
            <th width="100" nowrap="nowrap">EDIT</th>
            <th width="100" nowrap="nowrap">DELETE</th>
        </tr>

        <c:forEach var="task" items="${tasks}">
            <tr>
                <td>
                    <c:out value="${task.id}" />
                </td>
                <td>
                    <c:out value="${task.title}" />
                </td>
                <td>
                    <c:out value="${task.description}" />
                </td>
                <td>
                    <a href="/task/edit/${task.id}"><button>Edit</button></a>
                </td>
                <td>
                    <a href="/task/delete/${task.id}"><button>Delete</button></a>
                </td>
            </tr>
        </c:forEach>

    </table>
</div>

<div class="create_link-content">
    <a href="${pageContext.request.contextPath}/task/create/">
        <button>CREATE</button>
    </a>
</div>

<jsp:include page="../include/_footer.jsp"/>