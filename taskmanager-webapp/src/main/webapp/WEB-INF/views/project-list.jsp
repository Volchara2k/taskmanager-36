<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../include/_header.jsp"/>

<div class="list_header-content">
    <h1>Project list:</h1>
</div>

<din class="from_content-container">
    Task Enterprise Compose Up
    Ship is a good, but don't know who is it
    Golden shawty lel
    Promitozin in a hood is a good
    Servlet TCP/IP DESCRIPTION
    lel it's right
    it's okay xd

</div>

<div class="main-content">
    <table>

        <tr>
            <th width="200" nowrap="nowrap">ID</th>
            <th width="200" nowrap="nowrap">TITLE</th>
            <th width="100%" align="center">DESCRIPTION</th>
            <th width="100" nowrap="nowrap">EDIT</th>
            <th width="100" nowrap="nowrap">DELETE</th>
        </tr>

        <c:forEach var="project" items="${projects}">
            <tr>
                <td>
                    <c:out value="${project.id}" />
                </td>
                <td>
                    <c:out value="${project.title}" />
                </td>
                <td>
                    <c:out value="${project.description}"/>
                </td>
                <td>
                    <a href="/project/edit/${project.id}"><button>Edit</button></a>
                </td>
                <td>
                    <a href="/project/delete/${project.id}"><button>Delete</button></a>
                </td>
            </tr>
        </c:forEach>

    </table>
</div>

<div class="create_link-content">
    <a href="${pageContext.request.contextPath}/project/create/">
        <button>CREATE</button>
    </a>
</div>

<jsp:include page="../include/_footer.jsp"/>