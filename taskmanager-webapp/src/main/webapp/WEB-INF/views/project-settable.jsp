<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../include/_header.jsp"/>

<c:choose>
<c:when test="${not empty project.title}">

    <div class="project_activity-content">
        <h2>Edit project: &laquo;${project.title}&raquo;</h2>
    </div>

    <form:form action="/project/edit/${project.id}" method="POST" modelAttribute="project">

        <form:input type="hidden" path="id"/>
        <form:input type="hidden" path="userId"/>

        <div class="project_write-content" align="center">
            <div><p>Title:</p></div>
            <div><form:input type="text" path="title"/></div>
            <div><p>Description:</p></div>
            <div><form:input type="text" path="description"/></div>
            <button type="submit">SAVE</button>
        </div>

    </form:form>

</c:when>

<c:otherwise>

    <div class="project_activity-content">
        <h2>Create project</h2>
    </div>

    <form:form action="/project/create/" method="POST" modelAttribute="project">

        <form:input type="hidden" path="id"/>
        <form:input type="hidden" path="userId"/>

        <div class="project_write-content" align="center">
            <div><p>Title:</p></div>
            <div><form:input type="text" path="title"/></div>
            <div><p>Description:</p></div>
            <div><form:input type="text" path="description"/></div>
            <button type="submit">SAVE</button>
        </div>

    </form:form>

</c:otherwise>
</c:choose>

<jsp:include page="../include/_footer.jsp"/>``