<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Task Manager</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style type="text/css"><%@include file="../resources/styles.css"%></style>
    </head>

    <body>
        <main>
        <div class="container-content">
            <table class="list-table">
            <header>
                <tr>
                    <ul>
                      <li><a href="${pageContext.request.contextPath}/">Task Manager</a></li>
                      <li><a href="${pageContext.request.contextPath}/tasks/">Tasks</a></li>
                      <li><a href="${pageContext.request.contextPath}/projects/">Projects</a></li>
                    </ul>
                </tr>
            </header>
                <tr>
                    <td style="padding: 5%">