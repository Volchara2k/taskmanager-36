package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDomainService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserUnlimitedAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.file.InvalidDomainException;

import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskUserService taskUserService;

    @NotNull
    private final IProjectUserService projectUserService;

    @NotNull
    private final IUserUnlimitedAdapterService userUnlimitedAdapterService;

    @NotNull
    private final ITaskAdapterService taskAdapterService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    @SneakyThrows
    @Override
    public DomainDTO dataImport(@Nullable final DomainDTO domain) {
        if (Objects.isNull(domain)) throw new InvalidDomainException();
        this.userService.importRecords(
                domain.getUsers()
                        .stream()
                        .map(this.userUnlimitedAdapterService::toModel)
                        .collect(Collectors.toList())
        );

        this.taskUserService.importRecords(
                domain.getTasks()
                        .stream()
                        .map(this.taskAdapterService::toModel)
                        .collect(Collectors.toList())
        );

        this.projectUserService.importRecords(
                domain.getProjects()
                        .stream()
                        .map(this.projectAdapterService::toModel)
                        .collect(Collectors.toList())
        );

        return domain;
    }

    @SneakyThrows
    @Override
    public DomainDTO dataExport(@Nullable final DomainDTO domain) {
        if (Objects.isNull(domain)) throw new InvalidDomainException();

        domain.setUsers(
                this.userService.exportRecords()
                        .stream()
                        .map(this.userUnlimitedAdapterService::toDTO)
                        .collect(Collectors.toList())
        );

        domain.setTasks(
                this.taskUserService.exportRecords()
                        .stream()
                        .map(this.taskAdapterService::toDTO)
                        .collect(Collectors.toList())
        );

        domain.setProjects(
                this.projectUserService.exportRecords()
                        .stream()
                        .map(this.projectAdapterService::toDTO)
                        .collect(Collectors.toList())
        );

        return domain;
    }

}